# Extra Default Wallpapers

Project to create and maintain a set of extra default wallpapers that Fedora users can choose as an alternative to the default release wallpaper.

## Goal

We will produce at least 6 abstract wallpapers (with both light and dark versions for each) as a set for Fedora 37.

## Thumbs 

Here is how they came out for F37 Beta:

### Petals
<!-- ![Petals](https://gitlab.com/fedora/design/team/extra-default-wallpapers/-/raw/main/thumbs/wallpaper01-petals.png "Petals") -->
![Petals Updated](https://gitlab.com/fedora/design/team/release-artwork/extra-default-wallpapers/-/design_management/designs/473992/cf6e373640d06db393baba8ca4012977195d7855/raw_image "Petals Updated for Final")

### Flight
![Flight](https://gitlab.com/fedora/design/team/extra-default-wallpapers/-/raw/main/thumbs/wallpaper02-flight.png "Flight")

### Future City
<!-- ![Future City](https://gitlab.com/fedora/design/team/extra-default-wallpapers/-/raw/main/thumbs/wallpaper03-futurecity.png "Future City") -->
![Future City Updated](https://gitlab.com/fedora/design/team/release-artwork/extra-default-wallpapers/-/design_management/designs/473993/623e49b45b00892577e391e498260a5171e54ae1/raw_image "Future City Updated for Final")

### Glass Curtains
![Glass Curtains](https://gitlab.com/fedora/design/team/extra-default-wallpapers/-/raw/main/thumbs/wallpaper04-glasscurtains.png "Glass Curtains")

### Mermaid
![Mermaid](https://gitlab.com/fedora/design/team/extra-default-wallpapers/-/raw/main/thumbs/wallpaper05-mermaid.png "Mermaid")

### Montclair
<!-- ![Montclair](https://gitlab.com/fedora/design/team/extra-default-wallpapers/-/raw/main/thumbs/wallpaper06-montclair.png "Montclair") -->
![Montclair Updated](https://gitlab.com/fedora/design/team/release-artwork/extra-default-wallpapers/-/design_management/designs/473994/8f51fd5dc6e433b7bb5dc28a23c05d132b196de8/raw_image "Montclair Updated for Final")

## Todo/Timeline

* [x] Research what (if any) types of narrative themes tie together default wallpaper sets on various platforms (OS X, iOS, Android, Samsung, etc.)
* [x] Determine theme to use (if any) to connect designs
* [x] Brainstorm concepts
* [x] Sketch concepts
* [x] Start implementing in Blender
* [x] Map concepts to mockups
* [x] Finalize light & dark versions of mockups
* [x] Final selection and preparation of wallpapers
* [x] Package for F37 Beta
* [x] Solicit feedback 
* [x] Make any necessary / possible changes/tweaks for F37 Final
* [ ] (if necessary) Package for F37 Final

## Communications:

* [x] [Initial draft blog post](https://blog.linuxgrrl.com/2022/06/27/abstract-wallpapers-in-blender-using-geometry-nodes/)
* [x] [Shared to discussions.fpo](https://discussion.fedoraproject.org/t/fedora-extra-wallpapers-project/39962)
* [x] [Shared on Twitter](https://twitter.com/mairin/status/1541460444447215616) as well

## Specifications

* 4096x4096 - to either be cropped manually or cropped in software. This means design must work both cropped horizontally and vertically.
* Supports the day/night feature - a quick cross fade transition between the two - so each wallpaper will have a light version and a dark version
* Abstract - because of the aspect ratio and light/dark mode requirement, we'll start by making an abstract set
* webp format - it is smaller and GNOME is looking to move this way.

## Resources

* [GNOME HIG Background Guidelines](https://developer.gnome.org/hig/reference/backgrounds.html)
* jimmac's livestreams of creating the GNOME ones in Blender: https://www.twitch.tv/videos/1480042075 & https://www.twitch.tv/videos/1479336862
